# crm-web-reportcp

> Vue 2 + Ant Design, 兼容 IE 11

## CRM 客户管理卡模块

开发时, 请根据 `.env.local.example` 创建1个 `.env.local` 文件

## Project setup
```
npm install

npm需要切到公司内网的 NPM 上，具体操作如下
切换公司源：npm config set registry https://nexus.51job.com/repository/npm-public/
登录：npm login

Username deployment
Password wosdaHlpnYFL5VMV
Email 自己的邮箱（andy.liu@51job.com）
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



### 开发规范：
#### CSS规范：
公共全局样式：
全局覆盖ant样式：src/styles/antd.less
全局样式文件：src/styles/index.less

1. vue内的style模块，要加scoped，使用less
2. calss命名尽量有意义，class名自定义的用_（下划线）分割，例如 .customer_button_title，class名用于覆盖组件或者别的其他依赖包的，用-（中划线）分割，例如.ant-layout-header
3. 项目用到的所有颜色均使用颜色变量，为便于统一管理，均在antd.less中声明
#### JS规范
1. 请使用es6语法，变量/方法名用驼峰法命名
2. 所有判断必须是全集，有if就要有else , 有switch就要有default
4. 单个vue文件不应超过1000行，单个方法不应超过200行
5. 同一段代码如果超过两次以上使用，就要封装公共方法或者组件
6. 由于代码中使用了eslint规范，提交代码之前请用npm run lint格式化代码，保证版本代码格式统一

#### git 规范
1 提交代码前请运行npm run lint格式化代码