"use strict";
const env = process.env.NODE_ENV;
const webpack = require("webpack");
const path = require("path");

let publicPath = "/";
if (env !== "local") {
  publicPath = "./";
}
module.exports = {
  outputDir: "dist",
  assetsDir: "static",
  indexPath: "index.html",
  publicPath: publicPath,
  devServer: {
    port: 8089, // 端口号
  },
  css: {
    // CSS 预设器配置项
    loaderOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [
        // 全局变量路径
        path.resolve(__dirname, "./src/styles/variable.less"),
      ],
    },
  },

  // 开发环境的后端应当允许跨域

  configureWebpack: (config) => {
    // 开发时生成 source map
    if (env === "local") config.devtool = "source-map";
  },
  chainWebpack: (config) => {
    // moment.js 仅加载 zh-cn locale
    config
      .plugin("ContextReplacementPlugin")
      .use(webpack.ContextReplacementPlugin, [/moment[/\\]locale$/, /zh-cn/]);
    config.plugins.delete("preload");
    config.plugins.delete("prefetch");
    config.entry.app = ["babel-polyfill", "./src/main.js"];
  },
};
