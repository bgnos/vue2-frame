# 项目公共组件统计与用法介绍

### CrmTable

// query 接收一个函数，该函数返回一个 promise，fulfilled 状态的 promise 中返回的数据格式为 {list:列表数据,total:总数据量}
// query 变化时会自动调用 query 使用初始的 page 和 size 请求接口刷新页面
// 提供 refreshCurrentPage 用于仅刷新当前页的数据
// 也可以按 element 的原生用法使用，el-table 的其他功能按文档使用即可

// 列的配置:
// 1. 可以按 element-ui 的写法手写每一个 el-column-item
// 2. 也可以像 ant 中配置 columns，ant 中 column 的 title 和 dataIndex 可以不变（组件内部会对应传给 el-column-item prop 和 label ），也可以按 el-column-item 的配置写成 prop 和 label
// customRender 传入参数与 ant 中用法一致，而 scopedSlots: { customRender:"XXX" } 等于定义一个名为 XXX 的插槽，插槽直接传入 el-table-column 组件自定义

### CrmInput

```
// max-length 传字节数
<crm-input v-model='value' />
```

### StringCheckbox、NumberCheckbox

```
<string-checkbox v-model='value' /> '0'、'1'
<number-checkbox v-model='value' /> 0、1
```

### AmountInput

```
金额类型的输入框，自动格式化显示的值为保留两位小数
<amount-input v-model='value' />
```

### NumberInput

```
可输入范围 [0-9]
<number-input v-model='value' />
```

### CrmUpload

```
// attaches：Array<file>
<crm-upload @change="(attaches)=>{
  // to do something
}"></crm-upload>
```

### CrmCollapse

```
<crm-collapse title="title">content</crm-collapse> //支持 slot='title'
```

### PercentInput

```
可输入范围 [0-9\.], 且value<=1 || value >=0，保留四位小数
<percent-input v-model='value' />
```

### CrmPanel

```
<crm-panel title="title">content</crm-panel> //支持 slot='title'
```

### CrmSwitch

```
v-model ，快捷创建多样式开关组合、支持异步
（添加点击事件，通过事件处理函数的返回值决定是否切换状态，返回 truth 或 状态为 Fulfilled 的 Promise 切换状态，返回 Falsy 或 状态为 Rejected 的 Promise 不切换状态）

<crm-switch v-model='status' class='as-on-off' />
```
