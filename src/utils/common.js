import router from "@/router";
import { Message, Loading } from "./element";
import { downloadFile } from "@/api/download";
import downloadjs from "downloadjs";

export const fetchData = async (api, params, isLoading = true) => {
  if (isLoading) Loading.show();
  return api(params)
    .then((result) => {
      if (result.code == "0") {
        if (isLoading) Loading.close();
        return Promise.resolve(result?.data);
      } else {
        Message.error(result.message);
        return Promise.reject(result);
      }
    })
    .catch((err) => {
      if (isLoading) Loading.close();
      throw err;
    });
};

// 登出
export const loginOut = () => {
  const env = process.env.NODE_ENV;
  setTimeout(() => {
    if (env === "local") {
      router.push("/login");
    } else if (!/login/.test(location.hash)) {
      window.location.href = `/ecs/index.asp`;
    }
  }, 500);
};

export const toDownload = async (record) => {
  Loading.show();
  let fileInfo = record;
  fileInfo.filePath = record.filePath || record.url;
  fileInfo.fileName = record.fileName || record.name;

  try {
    const result = await downloadFile({
      filepath: record.filePath,
      token: record.token,
    });
    downloadAction(fileInfo, result);
  } catch {
    (err) => {
      Message.error(err?.message);
    };
  }
  Loading.close();
};

export const downloadAction = async (record, result) => {
  try {
    const filePath = record.filePath;
    const ext = filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase();
    const fileName = record.fileName.replaceAll("." + ext, "") + "." + ext;
    if (ext == "pdf" || ext == "png" || ext == "jpg" || ext == "jpeg") {
      let type = "";
      if (ext == "pdf") {
        type = "application/pdf";
      } else type = "image/" + ext;
      const binaryData = [];
      binaryData.push(result);
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(
          new Blob(binaryData, { type: type }),
          fileName
        );
      } else {
        window.open(
          window.URL.createObjectURL(new Blob(binaryData, { type: type }))
        );
      }
    } else {
      downloadjs(result, fileName);
    }
  } catch {
    (err) => {
      Message.error(err?.message);
    };
  }
};

export function deepTrim(obj) {
  if (!obj) return;
  return Object.keys(obj).reduce((pre, key) => {
    if (obj[key]?.toString === "[object Object]") {
      pre[key] = deepTrim(obj[key]);
    } else {
      pre[key] = typeof obj[key] === "string" ? obj[key].trim() : obj[key];
    }
    return pre;
  }, {});
}
