import moment from "moment";

// 金额保留两位小数格式化
export const amountRender = (amount) =>
  isNaN(Number(amount)) ? amount || "0.00" : Number(amount).toFixed(2);

// 年-月-日
export const dateRender = (date) => date && moment(date).format("YYYY-MM-DD");

//  年-月-日 时：分：秒
export const dateTimeRender = (date) =>
  date && moment(date).format("YYYY-MM-DD HH:mm:SS");

// 审批结果
export const approveResultRender = (val) => {
  let result = "";
  switch (val) {
    case "1":
      result = "审批通过";
      break;
    case "0":
      result = "审批驳回";
      break;
    default:
      result = "未审批";
      break;
  }
  return result;
};

// 审批时间
export const approveDateRender = (date, approveresult) => approveresult && date;
