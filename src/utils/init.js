import store from "@/store";
import { fetchData } from "./common";
import {
  getLoginUserCondition,
  getBranchList,
  getDictDd,
  getDict,
  getAreaProvince,
} from "@/api/common";

export function optionsInit() {
  initDefaultCondition();
  initBranchCondition();
  initThirdPlatformCondition();
  initPayStatusDict();
  initPayBatchStatusDict();
  initTopMenuDict();
  initAreaProvince();
}

function initDefaultCondition() {
  fetchData(getLoginUserCondition, {}, false).then((data) => {
    data.isFinance = !!Number(data.isFinance);
    store.commit("dictStore/updateState", {
      ...data,
    });
  });
}

function initBranchCondition() {
  fetchData(getBranchList, {}, false).then((data) => {
    store.commit("dictStore/updateState", {
      branchCondition: data,
    });
  });
}

function initThirdPlatformCondition() {
  fetchData(
    getDictDd,
    {
      dictname: "DD_ThirdPlatform",
      keymap: { validflag: "1" },
      valuename: ["code", "value"],
    },
    false
  ).then((data) => {
    store.commit("dictStore/updateState", {
      thirdPlatformCondition: {
        clearable: true,
        options: data,
      },
    });
  });
}

function initPayStatusDict() {
  let params = {
    names: "D_PayStatus",
    valid: 1,
  };
  fetchData(getDict, params, false).then((data) => {
    store.commit("dictStore/updateState", {
      payStatusCondition: {
        clearable: true,
        options: data[0].D_PayStatus.map((c) => ({
          ...c,
          code: c.value,
          value: c.label,
        })),
      },
    });
  });
}

function initPayBatchStatusDict() {
  let params = {
    names: "D_PayBatchStatus",
    valid: 1,
  };
  fetchData(getDict, params, false).then((data) => {
    store.commit("dictStore/updateState", {
      payBatchStatusCondition: {
        clearable: true,
        options: data[0].D_PayBatchStatus.map((c) => ({
          ...c,
          code: c.value,
          value: c.label,
        })),
      },
    });
  });
}

function initTopMenuDict() {
  let params = {
    names: "D_MainMenu",
    valid: 1,
  };
  fetchData(getDict, params, false).then((data) => {
    store.commit("dictStore/updateState", {
      topMenu: data[0].D_MainMenu.sort(
        (a, b) => Number(a.listIndex) - Number(b.listIndex)
      ),
    });
  });
}

function initAreaProvince() {
  fetchData(getAreaProvince, {}, false).then((data) => {
    store.commit("dictStore/updateState", {
      areaProvince: { options: data, clearable: true },
    });
  });
}
