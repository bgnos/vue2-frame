const { export_json_to_excel } = require("./excel/Export2Excel");
import { Message } from "../element";

/**
 *
 * @param {数据} tableData
 * @param {头部} tHeader
 * @param {} filterVal
 * @param {文件名} fileName
 * @returns
 */
export function exportToExcel(tableData, tHeader, filterVal, fileName) {
  return new Promise((resolve, reject) => {
    if (tableData != null && tableData.length !== 0) {
      require.ensure([], () => {
        // 上面的是tableData里对象的属性
        // 导出表格前的数据格式转换
        const list = tableData; // 把data里的tableData存到list
        const data = formatJson(filterVal, list);
        export_json_to_excel(tHeader, data, fileName);
      });
      resolve();
    } else {
      reject();
      Message.error("不存在要导出的数据！");
    }
  });
}

function formatJson(filterVal, jsonData) {
  return jsonData.map((v) => filterVal.map((j) => v[j]));
}
