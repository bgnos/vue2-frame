// 适用于 a-form-model 的各种 rule

import pattern from "../pattern";
import {
  validateIdCard,
  validateSocialId,
  verifyBankCardNumber,
} from "../validator";

// 自定义
export const ruleOfRequired = (message, trigger = "change") => ({
  required: true,
  message,
  trigger,
});

// 输入框必填
export const inputRequired = (label, trigger = ["change", "blur"]) => ({
  required: true,
  message: `请输入${label}`,
  trigger,
});

// 下拉框必选
export const selectRequired = (label, trigger = "change") => ({
  required: true,
  message: `请选择${label}`,
  trigger,
});

// 长度
export const ruleOfMaxLength = (label, max, trigger = ["change", "blur"]) => ({
  max,
  message: `${label}不能超过${max}个字节`,
  trigger,
});

// 正则
export const ruleOfPattern = (
  label,
  pattern,
  trigger = ["change", "blur"]
) => ({
  pattern: pattern,
  message: `${label}格式不正确`,
  trigger,
});

export const ruleOfPostcode = () => ruleOfPattern("邮编", pattern.postcode);

export const ruleOfPhone = () => ruleOfPattern("手机", pattern.phone);

export const ruleOfTel = () => ruleOfPattern("电话", pattern.tel);

export const ruleOfAreaCodeTel = () =>
  ruleOfPattern("（区号）电话", pattern.areacodetel);

export const ruleOfFax = () => ruleOfMaxLength("传真", 32);

export const ruleOfEmail = () => ruleOfPattern("邮箱", pattern.email);

export const ruleOfMemo = () => ruleOfMaxLength("备注", 200);

export const ruleOfMutipleEmail = () => ({
  pattern: pattern.moreEmail,
  message: `格式不正确,多个邮箱请用","隔开`,
  trigger: "blur",
});

// 身份证
export const ruleOfIdCard = () => ({
  validator: (rule, value, callback) => {
    if (!validateIdCard(value)) {
      callback(new Error("身份证号码格式不正确"));
      return;
    }
    callback();
  },
  trigger: ["change", "blur"],
});

// 统一信用代码
export const ruleOfSocialcreditcode = () => ({
  validator: (rule, value, callback) => {
    if (!validateSocialId(value)) {
      callback(new Error("统一社会信用代码不正确！"));
      return;
    }
    callback();
  },
  trigger: ["change", "blur"],
});

// 金额校验范围校验：默认只校验不能小于 0
export const ruleOfAmountInvalid = (label = "金额", min = 0.01, max) => ({
  validator: (rule, value, callback) => {
    if (max !== undefined && Number(value) > Number(max)) {
      callback(new Error(`${label}"不能大于 ${Number(max).toFixed(2)} ！"`));
      return;
    }
    if (Number(value) < Number(min)) {
      callback(new Error(`${label}"不能小于 ${0.01} ！"`));
      return;
    }
    callback();
  },
  trigger: ["blur"],
});

// 必填+长度组合
export const requiredAndMaxLength = (label, max = 100) => [
  inputRequired(label),
  ruleOfMaxLength(label, max),
];

// 银行卡
export const ruleOfBankCard = (label = "银行卡号") => ({
  validator: (rule, value, callback) => {
    if (!verifyBankCardNumber(value)) {
      callback(new Error(label + "格式不正确"));
      return;
    }
    callback();
  },
  trigger: ["change", "blur"],
});
