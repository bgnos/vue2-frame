const SIDER_MENU = [
  {
    title: "项目管理",
    icon: "iconfont icon-project",
    submenus: [
      // {
      //   title: "项目列表",
      //   path: "/project/list",
      // },
      {
        title: "预审列表",
        path: "/preaudit/list",
      },
    ],
  },
  {
    title: "供应商管理",
    icon: "iconfont icon-supplier",
    href:
      (process.env.NODE_ENV === "production"
        ? location.origin
        : process.env.VUE_APP_OLD_CRM_HTTP) +
      process.env.VUE_APP_SUPPLIER_PATH +
      "suppliermanage/supplierlist",
  },
  {
    title: "供应商合同系统",
    icon: "iconfont icon-supplier",
    href:
      (process.env.NODE_ENV === "production"
        ? location.origin
        : process.env.VUE_APP_OLD_CRM_HTTP) + process.env.VUE_APP_SUPPLIER_PATH,
  },
];
export default {
  namespaced: true,
  state: () => ({
    menu: SIDER_MENU,
  }),
};
