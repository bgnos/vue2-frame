import VueCookies from "vue-cookies";

const env = process.env.NODE_ENV;
const crmSysKey = "CRM";
const nCrmSysKey = "NCRM-SERVICE";
const userInfoKey = "CRMPLUS";
const JSESSIONIDKey = "JSESSIONID";

if (env !== "local") {
  VueCookies.config("1D", "/crm");
}

//解析cookie
function decodeCookie(value) {
  let userInfo = {};
  let val;
  if (value) {
    val = value.replaceAll("%", "%25");
  }
  const decodeValueArr = decodeURIComponent(val).split("&");
  decodeValueArr.forEach((item) => {
    const splitItem = item.split("=");
    if (splitItem[0] == "cname") {
      splitItem[1] = decodeURI(splitItem[1]);
    }
    userInfo[splitItem[0]] = splitItem[1];
  });
  return userInfo;
}
function getCookie(name) {
  var value = VueCookies.get(name);
  let userInfo = {};
  if (value && value != "") {
    userInfo = decodeCookie(value);
    VueCookies.set(JSESSIONIDKey, userInfo.sessionid);
  }
  return userInfo || {};
}

const userInfo = getCookie(userInfoKey) || {};
export const actions = {
  UPDATE_USER_INFO: "updateUserInfo",
};
export default {
  namespaced: true,
  state: () => ({
    userInfo: userInfo,
    // branchCode: "SH", // 全局branchCode
  }),
  getters: {
    isEc: (state) => state.userInfo.branch == "EC",
  },
  mutations: {
    setUserInfo(state, userInfo) {
      state.userInfo = decodeCookie(userInfo);
      if (userInfo) {
        VueCookies.set(userInfoKey, userInfo);
        VueCookies.set(JSESSIONIDKey, state.userInfo.sessionid);
      } else {
        VueCookies.set(userInfoKey, "");
        VueCookies.set(JSESSIONIDKey, "");
        VueCookies.set(crmSysKey, "");
        VueCookies.set(nCrmSysKey, "");
      }
    },
  },
  //调用mutations方法，可以使用异步方法
  actions: {
    // TODO fetchSiderMenu

    updateUserInfo: async ({ commit }, userInfo) => {
      commit("setUserInfo", userInfo);
    },
  },
};
