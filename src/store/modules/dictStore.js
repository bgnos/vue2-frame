import DeepClone from "lodash/cloneDeep";

export const actions = {
  DICT_INIT: "dictInit",
};
export default {
  namespaced: true,
  state: () => ({
    conditionInited: false,
    isFinance: undefined,

    // 项目类型
    proTypeCondition: { options: [] },
    // 城市
    branchCondition: { options: [] },
    // 报销类型
    settleTypeCondition: { options: [] },
    // 付款单状态
    paymentStatusCondition: {
      clearable: true,
      options: [
        { value: "已生成", code: "01" },
        { value: "已撤单", code: "02" },
        { value: "已作废", code: "03" },
      ],
    },

    // 借支类型
    borrowTypeCondition: {
      clearable: true,
      options: [
        { value: "供应商", code: "01" },
        { value: "个人", code: "02" },
      ],
    },

    // 报销审核-税率
    costRates: [
      { code: "0.01", value: "1%" },
      { code: "0.02", value: "2%" },
      { code: "0.03", value: "3%" },
      { code: "0.04", value: "4%" },
      { code: "0.05", value: "5%" },
      { code: "0.06", value: "6%" },
      { code: "0.09", value: "9%" },
      { code: "0.10", value: "10%" },
      { code: "0.11", value: "11%" },
      { code: "0.13", value: "13%" },
      { code: "0.16", value: "16%" },
      { code: "0.17", value: "17%" },
    ],
    // 报销审核-发票类型
    invTypes: [
      { value: "01", label: "增值税普通发票" },
      { value: "02", label: "增值税专用发票" },
    ],

    // 第三方平台类型
    thirdPlatformCondition: { options: [] },
    // 第三方平台付款状态-支付列表
    payStatusCondition: { options: [] },
    // 第三方批次状态-支付批次列表
    payBatchStatusCondition: { options: [] },
    //顶部菜单
    topMenu: [
      {
        title: "1",
      },
      {
        title: "2",
      },
      {
        title: "3",
      },
      {
        title: "4",
      },
      {
        title: "5",
      },
      {
        title: "6",
      },
      {
        title: "7",
      },
      {
        title: "8",
      },
      {
        title: "9",
      },
      {
        title: "10",
      },
      {
        title: "11",
      },
      {
        title: "12",
      },
      {
        title: "13",
      },
    ],
    //省份+直辖市
    areaProvince: { options: [] },
  }),
  getters: {
    payTypeCondition(state) {
      const codes = ["01", "02", "03", "04"];
      const result = DeepClone(state.settleTypeCondition);
      result.options = result.options.filter(
        (c) => codes.indexOf(c.code) != -1
      );
      return result;
    },
  },
  mutations: {
    updateState: (state, obj) => {
      Object.keys(obj).forEach((key) => {
        state[key] = obj[key];
      });
    },
  },
  //调用mutations方法，可以使用异步方法
  actions: {},
};
