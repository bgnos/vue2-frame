import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

// 自动化导入子模块，子模块名称为文件名(增加子模块时，保持子模块和文件名统一)
const files = require.context("./modules", false, /\.js$/);
const modules = {};
files.keys().forEach((key) => {
  const moduleName = key.replace(/\.\//, "").replace(/\.js/, "");
  const store = files(key).default;
  modules[moduleName] = store;
  modules[moduleName].namespaced = true;
});

export default new Vuex.Store({
  //如果不是生产环境，启用严格模式，所有修改更新参数必须从mutations调用
  strict: process.env.NODE_ENV !== "production",
  state: {},
  mutations: {},
  actions: {},
  modules: {
    ...modules,
  },
});
