// 集中管理需要全局注册的组件

import SubMenu from "@/layout/Sider/components/SubMenu";
import MenuItem from "@/layout/Sider/components/MenuItem";
import MenuItemWrap from "@/layout/Sider/components/MenuItemWrap";

import CrmInput from "./element/CrmInput";
import CrmSelect from "./element/CrmSelect";
import DictSelect from "./DictSelect";
import AmountInput from "./element/AmountInput";
import PercentInput from "./element/PercentInput";
import CrmCollapse from "./element/CrmCollapse";
import CrmTable from "./element/CrmTable";
import CrmUpload from "./element/CrmUpload";
import NumberCheckbox from "./element/NumberCheckbox";
import StringCheckbox from "./element/StringCheckbox";
import NumberInput from "./element/NumberInput";
import CrmSwitch from "./element/CrmSwitch";
import CrmDateRange from "./element/CrmDateRange";
import CrmTabs from "./CrmTabs";

import CrmPanel from "./CrmPanel";

export default function (Vue) {
  Vue.component(SubMenu.name, SubMenu);
  Vue.component(MenuItem.name, MenuItem);
  Vue.component(MenuItemWrap.name, MenuItemWrap);

  Vue.component(CrmInput.name, CrmInput);
  Vue.component(CrmSelect.name, CrmSelect);
  Vue.component(DictSelect.name, DictSelect);
  Vue.component(AmountInput.name, AmountInput);
  Vue.component(PercentInput.name, PercentInput);
  Vue.component(CrmCollapse.name, CrmCollapse);
  Vue.component(CrmTable.name, CrmTable);
  Vue.component(CrmUpload.name, CrmUpload);
  Vue.component(NumberCheckbox.name, NumberCheckbox);
  Vue.component(StringCheckbox.name, StringCheckbox);
  Vue.component(NumberInput.name, NumberInput);
  Vue.component(CrmSwitch.name, CrmSwitch);
  Vue.component(CrmDateRange.name, CrmDateRange);
  Vue.component(CrmTabs.name, CrmTabs);

  Vue.component(CrmPanel.name, CrmPanel);
}
