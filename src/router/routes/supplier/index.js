// meta.isListPage: 用于标识是否是列表页面，是列表页面则进行缓存处理，且不展示二面包屑中的项目信息
export default [
  {
    path: "/supplier",
    name: "Supplier",
    redirect: "/supplier/list",
    meta: { breadcrumb: "供应商列表" },
    component: () => import("@/layout"),
    children: [
      {
        path: "list",
        name: "SupplierList",
        meta: { isListPage: true },
        component: () => import("@/views/Supplier"),
      },
    ],
  },
];
