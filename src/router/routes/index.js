import basicRoutes from "./basic";
import supplierRoutes from "./supplier";

export default [...basicRoutes, ...supplierRoutes];
