import { amountRender, dateRender, dateTimeRender } from "@/utils";

function percentFormat(value) {
  return isNaN(Number(value)) ? value : (value * 100).toFixed(2) + "%";
}

export default function install(Vue) {
  Vue.filter("amountFormat", amountRender);
  Vue.filter("dateFormat", dateRender);
  Vue.filter("dateTimeFormat", dateTimeRender);
  Vue.filter("percentFormat", percentFormat);
}
