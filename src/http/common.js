import { service } from "./utils/serviceGenerator";
import httpTemplate from "./utils/indexTemplate";

const { post, get, put, upload, blobPost, blobGet } = httpTemplate(
  service,
  "common/"
);

export { post, get, put, upload, blobPost, blobGet };
export default { post, get, put, upload, blobPost, blobGet };
