import "@babel/polyfill";
import "normalize.css/normalize.css";
import "element-ui/lib/theme-chalk/index.css";
import "./styles/index.less";

import Vue from "vue";
// import ElementUI from "element-ui";

import App from "./App.vue";
import router from "./router";
import store from "./store";
import components from "./components";
import plugins from "./plugins";
import filters from "./filters";
import directive from "./directive";

// Vue.use(ElementUI);

Vue.use(components);
Vue.use(plugins);
Vue.use(filters);
Vue.use(directive);

Vue.config.errorHandler = function (err, vm, info) {
  if (
    (/cancel/i.test(err) && /(Promise\/async)/i.test(info)) ||
    /\[ElementForm\]unpected width/i.test(err)
  ) {
    // 忽略 Promise 不写 catch 报的错
  } else {
    console.error(err);
  }
};

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
