import { get } from "@/http/auth";

const defaultParams = {
  domain: location.origin + location.pathname,
  project: "crm-standard-project",
};

export const pageAuthCheck = (params) =>
  get("/auth/check", { ...params, ...defaultParams });
export const recorduserlog = (params) =>
  get("/auth/recorduserlog", { ...params, ...defaultParams });
