import { post, get } from "@/http/supplier";
import qs from "qs";

// 供应商列表
export const getList = (params) => post("list", params);

// 新增供应商
export const addSupplier = (params) => post("add", params);
// 编辑供应商
export const editSupplier = (params) => post("edit", params);

// 启用供应商
export const enable = (params) => post("enable", params);
// 禁用供应商
export const disable = (params) => post("disable", params);

// 查看附件列表

export const lookFile = (params) => post("attachment/list", params);

// 供应商附件上传

export const uploadFile = (query, params) => {
  return post(`attachment/upload?${qs.stringify(query)}`, params);
};

// 供应商账号列表
export const getAccountList = (params) => post("account/list", params);

// 启用供应商账号
export const enableAccount = (params) => post("account/enable", params);

// 禁用供应商账号
export const disableAccount = (params) => post("account/disable", params);

// 新增供应商账号
export const addSupplierAccount = (params) => post("account/add", params);

// 编辑供应商账号
export const editSupplierAccount = (params) => post("account/edit", params);

// 删除附件
export const deleteFile = (params) => get("/attachment/delete", params);
