import { get, post } from "@/http";
import { get as commonGet } from "@/http/common";
import { post as commonPost } from "@/http/common";

//  报销批次列表查询条件选项
export const getSettleBatchApplyListCondition = (params) =>
  get("/search/getSettleBatchApplyListCondition", params);

// 费用类别选项
export const getFeeAmountTypeList = (params) =>
  get("/search/getFeeAmountTypeList", params);

// 查询内部员工信息
export const getEhrInfo = (params) => get("/search/getEhrInfo", params);

// 查询内部员工信息列表
export const getEhrInfoList = (params) =>
  post("/search/getEhrInfoList", params);

// 获取当前项目类型下的所有供应商信息，账户信息
export const getSupplier = (params) => get("/search/selectSupplierNew", params);

// 获取城市地区
export const getBranchList = (params) => get("/search/getBranchList", params);

// getDict
export const getDict = (params) => commonGet("/dict/get/dict", params);

export const getDictDd = (params) => commonPost("/dict/get/dict/dd", params);

//根据用户账户类型获取城市地区
export const getUserBranchList = (params) =>
  get("/search/getUserBranchList", params);

//获取 支付-我司 类型字典
export const getPayTypeList = (params) => get("/search/getPayTypeList", params);

// 获取项目中当前用户身份对应通用查询条件
export const getLoginUserCondition = (params) =>
  get("/search/getLoginUserCondition", params);

//查询第三方支付平台列表
export const getThirdPlatFormList = (params) =>
  get("/search/getThirdPlatFormList", params);

// 交接清单列表-获取报销类型字典
export const getSettleTypeList = (params) =>
  get("/search/getSettleTypeList", params);

// 获取省份+直辖市
export const getAreaProvince = (params) => post("/area/getProvince", params);
