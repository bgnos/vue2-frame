import { blobPost } from "@/http/finance";

// 下载文件
export const downloadFile = (params) => blobPost("/download/hq/file", params);
