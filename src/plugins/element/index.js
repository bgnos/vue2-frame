import {
  Pagination,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Menu,
  Submenu,
  MenuItem,
  Input,
  Radio,
  RadioGroup,
  Checkbox,
  CheckboxGroup,
  Select,
  Option,
  Button,
  Table,
  TableColumn,
  DatePicker,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Upload,
  Breadcrumb,
  BreadcrumbItem,
  Collapse,
  CollapseItem,
  MessageBox,
  Notification,
  Tooltip,
  Popover,
} from "element-ui";

import { Message, Loading } from "@/utils";

export default function install(Vue) {
  Vue.use(Pagination);
  Vue.use(Dropdown);
  Vue.use(DropdownMenu);
  Vue.use(DropdownItem);
  Vue.use(Menu);
  Vue.use(Submenu);
  Vue.use(MenuItem);
  Vue.use(Input);
  Vue.use(Radio);
  Vue.use(RadioGroup);
  Vue.use(Checkbox);
  Vue.use(CheckboxGroup);
  Vue.use(Select);
  Vue.use(Option);
  Vue.use(Button);
  Vue.use(Table);
  Vue.use(TableColumn);
  Vue.use(DatePicker);
  Vue.use(Form);
  Vue.use(FormItem);
  Vue.use(Tabs);
  Vue.use(TabPane);
  Vue.use(Upload);
  Vue.use(Breadcrumb);
  Vue.use(BreadcrumbItem);
  Vue.use(Collapse);
  Vue.use(CollapseItem);
  Vue.use(Tooltip);
  Vue.use(Popover);

  Vue.prototype.$msgbox = MessageBox;
  Vue.prototype.$alert = MessageBox.alert;
  Vue.prototype.$confirm = MessageBox.confirm;
  Vue.prototype.$prompt = MessageBox.prompt;
  Vue.prototype.$notify = Notification;

  Vue.prototype.$message = Message;
  Vue.prototype.$loading = Loading;
}
