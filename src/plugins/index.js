import DialogPlugin from "./dialog";
import Element from "./element";
import Publish from "./publish";

export default function (Vue) {
  Vue.use(DialogPlugin);
  Vue.use(Element);
  Vue.use(Publish);
}
