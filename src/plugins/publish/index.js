export default function install(Vue) {
  const env = process.env.NODE_ENV;
  Vue.prototype.$imgHttp = "/";
  if (env !== "local") {
    Vue.config.productionTip = false;
    Vue.prototype.$imgHttp = "./";
  }

  //请求后台地址
  Vue.prototype.$baseURL =
    env === "production" ? window.g.baseURL : process.env.VUE_APP_BASE_URL;

  //老系统前端地址
  Vue.prototype.$oldCrmHttp =
    env === "production" ? location.origin : process.env.VUE_APP_OLD_CRM_HTTP;

  //供应商合同审批系统
  Vue.prototype.$contactHTTP =
    Vue.prototype.$oldCrmHttp + process.env.VUE_APP_SUPPLIER_PATH;

  // //sales前端地址
  // Vue.prototype.$salesHttp =
  //   env === "production" ? window.g.salesHTTP : process.env.VUE_APP_SALES_HTTP;
  // //客户管理卡前端地址
  // Vue.prototype.$accountCardHttp =
  //   env === "production"
  //     ? window.g.accountHTTP
  //     : process.env.VUE_APP_ACCOUNT_CARD_HTTP;
  // //finance前端地址
  // Vue.prototype.$financeHttp =
  //   env === "production"
  //     ? window.g.financeHTTP
  //     : process.env.VUE_APP_FINANCE_HTTP;

  // //Internal地址
  // Vue.prototype.$internalHttp =
  //   env === "production"
  //     ? window.g.internalHttp
  //     : process.env.VUE_APP_INTERNAL_IP;
}
