export function handleBodyScroll(status, dialogCount, beforeBodyWidth) {
  const body = document.body;
  if (status == "close") {
    if (dialogCount === 0) {
      body.classList.remove("el-popup-parent--hidden");
      body.removeAttribute("style");
    }
  } else {
    const isScroll = body.clientHeight < body.scrollHeight;
    body.classList.add("el-popup-parent--hidden");
    if (isScroll) {
      body.setAttribute(
        "style",
        "padding-right:" + (document.body.clientWidth - beforeBodyWidth) + "px"
      );
    }
  }
}
