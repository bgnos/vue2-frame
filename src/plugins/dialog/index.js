import dialog from "./dialog";
import DialogWrap from "./component/dialog-wrap.vue";

function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Vue.prototype.$openDialog = function (comp, propsData, callback) {
    return dialog(Vue, comp, propsData, this, callback);
  };

  Vue.component("dialog-wrap", DialogWrap);

  Vue.mixin({
    methods: {
      $closeDialog(...arg) {
        this.$emit("close", ...arg);
      },
    },
  });

  const dialogContainer = document.createElement("div");
  dialogContainer.setAttribute("id", "dialog");
  document.body.appendChild(dialogContainer);
}

// auto plugin install
let GlobalVue = null;
if (typeof window !== "undefined") {
  GlobalVue = window.Vue;
} else if (typeof global !== "undefined") {
  GlobalVue = global.vue;
}
if (GlobalVue) {
  GlobalVue.use({
    install,
  });
}

// export default
export default {
  install,
};
