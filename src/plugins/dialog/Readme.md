### 异步风格的 Dialog & 无需关心弹窗嵌套问题 & 每次打开都是重新渲染（生命周期可以正常使用）

##### 建议 dialog-wrap 和 $openDialog、$closeDialog 一起使用，但也可以在弹窗组件中的 template 编写任意想展示的内容

#### Global Methods

- $openDialog
  - params: this.$openDialog(Dialog,props), Dialog 为组件组件的配置对象，既单文件组件默认导出的对象，props 为父组件需要传递给子组件的 props 数据
- $closeDialog
  - params：this.$openDialog(data), data 为弹窗关闭后传递给父组件的数据

#### dialog-wrap attrs
- title
  - type:String
  - 作用：弹窗标题
- closeOnClickModal
  - type:Boolean
  - 作用：点击蒙层时是否关闭弹窗（默认为 true ）
- onConfirm：
  - type:Function
  - 作用：没有使用 slot="footer" 时点击默认的 ‘确定’ 按钮时的回调函数，注意：该函数的结果将影响弹窗是否关闭（或需要关闭时手动调用 $closeDialog 方法），返回 Truthy 或 fulfilled 状态的 Promise 则关闭弹窗，其他不关闭
- onConfirm：
  - type:Function
  - 作用：没有使用 slot="footer" 时点击默认的 ‘取消’ 按钮时的回调函数，注意：该函数的结果将影响弹窗是否关闭（或需要关闭时手动调用 $closeDialog 方法），返回 Truthy 或 fulfilled 状态的 Promise 则关闭弹窗，其他不关闭

#### dialog-wrap slots
- 具名插槽
  - title
  - footer
- 默认插槽：弹窗主内容


#### example 

##### DialogComponent.vue

```
<template>
  <dialog-wrap title='dialogTitle' :onConfirm='onConfirm' >
    dialog-content:{{ name }}
  </dialog-wrap>
</template>

<script>

export default {
  name:"DialogComponent",
  props:{
    name:{
      type:String,
      default:''
    }
  },
  methods:{
    onConfirm(){
      // return false ;// don't close Dialog 
      const params = { name:"child" }
      return params ;// close Dialog 
    }
  }
}
</script>
```

##### Index.vue(需要打开 DialogComponent 的（父）组件)

```
<template>
  ......
</template>

<script>

import DialogComponent from 'xxx/DialogComponent.vue'

export default {
  name:"ParentComponent",
  ......
  methods:{
    openDialogComponent(){
      const childProps = { name:"parent" }
      this.$openDialog(DialogComponent,childProps).then((data)=>{
        console.log(data);// { name:"child" }
      })
    }
  }
}
</script>
```
