import Element from "./element";

export default function (Vue) {
  Vue.use(Element);
}
