import Loading from "./loading";

export default function (Vue) {
  Vue.use(Loading);
}
